
|import |
|guarabot|

!|EnvFixture|
|target url?|api token?|
|$VALUE=    |$VALUE=  |
*!

El cupo maximo es 300 porque esa es la capacidad máxima de aulas.
Dado que todos los laboratorios tienen proyector no es válido que una materia requiere ambas cosas (laboratorio y proyector)
Protocolo:
ok => status=201 {"resultado":"materia_creada"}
error => status=400 {"resultado:"error:...."}  (los puntos representan el mensaje especifico de error)


!2 Alta materia

!|AltaMateriaFixture|
|# caso   | codigo |  nombre        | docente        | cupo | modalidad  | con proyector | con laboratorio | valido? |
|  am1: caso correcto 1    |  9521  |  memo2         | Nico Paez      |  30  | parciales  |      si       |       no        |   si,materia_creada    |
|  am2: invalido cupo excedido    |  7509  |  memo1         | Sergio Vi      |  500 | parciales  |      si       |       no        |   no,cupo_excedido    |
|  am3: invalido proy y labo    |  7541  |  algo1         | Rosita W       |  500 | parciales  |      si       |       si        |   no,pedidos_incompatibles    |

!3 am4: el codigo de materia es unico
!|script         |FlujoBasicoFixture                                                                       |
|alta materia    | Memo2|codigo  | 9521     |Docente|Linus Torvalds|cupo|2|modalidad|parciales|
|alta materia    | Memo1|codigo  | 9521     |Docente|Linus Torvalds|cupo|2|modalidad|parciales|
|check           | status         | 400 |
|check           | error          | MATERIA_DUPLICADA |



!2 Inscripciones


!3 i1: un alumno no puede inscribirse dos veces en la misma materia
!|script         |FlujoBasicoFixture                                                                       |
|alta materia    | Memo2|codigo  |1001     |Docente|Linus Torvalds|cupo|2|modalidad|parciales|
|inscribir alumno|Juan Perez   |usernameAlumno|juanperez|materia|1001                                      |
|inscribir alumno|Juan Perez   |usernameAlumno|juanperez|materia|1001                                      |
|check           | status         | 400 |
|check           | error          | INSCRIPCION_DUPLICADA |

!3 i2: no pueden aceptarse inscripciones que superen el cupo
!|script         |FlujoBasicoFixture                                                                       |
|alta materia    | Memo2|codigo  |1001     |Docente|Linus Torvalds|cupo|2|modalidad|parciales|
|inscribir alumno| Juan Perez     |usernameAlumno| juanperez    |materia|1001|
|inscribir alumno| Matias Perez   |usernameAlumno| matiasperez  |materia|1001|
|inscribir alumno| Matias Firpo   |usernameAlumno| matiasfirpo  |materia|1001|
|check           | status         | 400 |
|check           | error          | CUPO_COMPLETO |


!3 i3: no pueden aceptarse inscripciones que superen el cupo
!|script         |FlujoBasicoFixture                                                                       |
|alta materia    | Memo2|codigo  |1001     |Docente|Linus Torvalds|cupo|2|modalidad|parciales|
|inscribir alumno| Juan Perez     |usernameAlumno| juanperez    |materia|1001|
|inscribir alumno| Matias Perez   |usernameAlumno| matiasperez  |materia|1001|
|inscribir alumno| Matias Firpo   |usernameAlumno| matiasfirpo  |materia|1001|
|check           | status         | 400 |
|check           | error          | CUPO_COMPLETO |


!3 i4: Inscripcion creada correctamente
!|script         |FlujoBasicoFixture                                                                       |
|alta materia    | Memo2|codigo  |1001     |Docente|Linus Torvalds|cupo|2|modalidad|parciales|
|inscribir alumno| Juan Perez     |usernameAlumno| juanperez    |materia|1001|
|check           | status         | 201 |
|check           | resultado      | inscripcion_creada |

!2 Consulta oferta academica

!3 Una materia
!|script         |OfertaAcademicaFixture                                                                       |
|alta materia    | Memo2|codigo  |1001     |Docente|Linus Torvalds|cupo|2|modalidad|parciales|
|consultar oferta|
|check           | cantidad de materias en la oferta academica | 1 |
|check           | oferta academica incluye codigo | 1001 | true |

!3 Dos materias
!|script         |OfertaAcademicaFixture                                                                       |
|alta materia    | Algo3 |codigo  |7507     |Docente|Carlos Fontela|cupo|50|modalidad|parciales|
|alta materia    | TDD   |codigo  |7510     |Docente|Emilio Gutter |cupo|60|modalidad|coloquio |
|consultar oferta|
|check           | cantidad de materias en la oferta academica | 2 |
|check           | oferta academica incluye codigo | 1001 | false |
|check           | oferta academica incluye codigo | 7507 | true  |
|check           | oferta academica incluye codigo | 7510 | true  |
